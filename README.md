# Exercise 1.3
This is a solution to exercise 1.3 (Concurrent Search) of the hands-on training of lecture **distributed systems**.

The folder contains a gradle project.
The project implements a simple concurrent search algorithm (see exercise for details).

Pass the following parameters when starting the program:
- `args[0]: Size of list N`
- `args[1]: Element to search for n`
- `args[2]: Number of threads p`

Start the iterative server from a CLI (like bash, cmd, powershell) as follows:
* `$ gradle run --args="50000000 80 8"`

That will work, if gradle is installed on your system, otherwise use the gradle wrapper contained in the folder (gradlew.bat, gradlew).

In this example the algorithm uses 8 threads to find the positions of number 8 in a list of numbers of size 50.000.000.

In the end, the programm will print the result and the running time to the screen.


