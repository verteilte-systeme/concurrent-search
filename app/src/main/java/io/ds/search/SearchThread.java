package io.ds.search;

import java.util.Collection;
import java.util.List;

public class SearchThread extends Thread {
    private int id;
    private Collection<Integer> results;
    private List<Integer> numbers;
    private int n;
    private int p;

    public SearchThread(int id, Collection<Integer> results, List<Integer> numbers, int n, int p) {
        this.id=id;
        this.results=results;
        this.numbers=numbers;
        this.n=n;
        this.p=p;
    }

    @Override
    public void run() {
        //calculate range
        int offset = numbers.size()/p;
        int from = id*offset;
        int to = from+offset;
        if (id==p) to+=numbers.size()%p;//the last thread takes the rest, too!

        //search n in range
        System.out.printf("Thread %d: from %d to %d.\n",id, from, to);
        for (int pos=from; pos<to; pos++) {
            if (numbers.get(pos)==n) {
                results.add(pos);//Found p at position pos!
            }
        }        
    }  
}
