package io.ds.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class App {
    /*
     * args[0]: length N of list. args[1]: element n to search for. args[2]: number
     * of threads p.
     */
    public static void main(String[] args) {
        if (args.length < 3) {
            System.out.println("Programm needs three parameter: N, n and p");
            System.exit(1);
        }
        int N = Integer.parseInt(args[0]);
        int n = Integer.parseInt(args[1]);
        int p = Integer.parseInt(args[2]);

        // Create list of length N with random numbers (values between 0 .. N-1)
        List<Integer> numbers = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < N; i++) {
            numbers.add(random.nextInt(N));
        }
        // Create synchronized result list
        Collection<Integer> results = Collections.synchronizedCollection(new ArrayList<>());

        long start = System.currentTimeMillis(); //time stamp

        // Create and start threads
        Thread[] threads = new Thread[p];
        for (int i = 0; i < p; i++) {
            threads[i] = new SearchThread(i, results, numbers, n, p);
            threads[i].start();
        }
        // Join threads
        for (int i = 0; i < p; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {//nothing to do
            }
        }
        long stop = System.currentTimeMillis();//time stamp
        
        // Print results
        System.out.printf("I found %d in the following positions %s\n", n, results);
        System.out.printf("%d ms\n", stop - start);
    }
}
